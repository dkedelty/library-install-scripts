#!/bin/bash
set -e

#SBATCH -n 1                         # number of cores
#SBATCH -t 0-12:00                   # wall time (D-HH:MM)
#SBATCH -o slurm.%j.out              # STDOUT (%j = JobId)
#SBATCH -e slurm.%j.err              # STDERR (%j = JobId)

module load cmake/3.2.1 mvapich2/2.3b-gnu-6x

BUILD_DIR=../Build/trilinos-12.10.1-Source/build/

cd $BUILD_DIR

if [ -f ./CMakeCache.txt ]; then
    rm ./CMakeCache.txt
fi
cmake \
 -D CMAKE_BUILD_TYPE:STRING=RELEASE \
 -D TPL_ENABLE_MPI:BOOL=ON \
 -D Trilinos_ENABLE_Fortran:BOOL=ON \
 -D Trilinos_ENABLE_Epetra:BOOL=ON \
 -D Trilinos_ENABLE_AztecOO:BOOL=ON \
 -D Trilinos_ENABLE_Teuchos:BOOL=ON \
 -D Trilinos_ENABLE_ML:BOOL=ON \
 -D Trilinos_ENABLE_TESTS:BOOL=OFF \
 -D Trilinos_ENABLE_ALL_OPTIONAL_PACKAGES:BOOL=OFF \
 -D CMAKE_C_COMPILER:PATH=mpicc \
 -D CMAKE_CXX_COMPILER:PATH=mpicxx  \
 -D CMAKE_Fortran_COMPILER:PATH=mpifort \
 -D TPL_ENABLE_BLAS:BOOL=ON \
 -D TPL_ENABLE_LAPACK:BOOL=ON \
 -D TPL_ENABLE_HYPRE:BOOL=ON \
 -D TPL_ENABLE_METIS:BOOL=ON \
 -D TPL_ENABLE_ParMETIS:BOOL=ON \
 -D TPL_BLAS_LIBRARIES:PATH=$HOME/agave/opt/lapack-3.7.0/libblas.a \
 -D TPL_LAPACK_LIBRARIES:PATH=$HOME/agave/opt/lapack-3.7.0/liblapack.a \
 -D TPL_HYPRE_LIBRARIES=$HOME/agave/opt/hypre-2.0.0/lib/libHYPRE.a \
 -D TPL_HYPRE_INCLUDE_DIRS=$HOME/agave/opt/hypre-2.0.0/include/ \
 -D TPL_METIS_LIBRARIES=$HOME/agave/opt/metis-5.1.0/lib/libmetis.a \
 -D TPL_METIS_INCLUDE_DIRS=$HOME/agave/opt/metis-5.1.0/include/ \
 -D TPL_ParMETIS_LIBRARIES=$HOME/agave/opt/parmetis-4.0.3/lib/libparmetis.a \
 -D TPL_ParMETIS_INCLUDE_DIRS=$HOME/agave/opt/parmetis-4.0.3/include/ \
 -D CMAKE_C_FLAGS:STRING="-O3" \
 -D CMAKE_CXX_FLAGS:STRING="-O3" \
 -D CMAKE_Fortran_FLAGS:STRING="-O3" \
 -D DART_TESTING_TIMEOUT:STRING=600 \
 -D CMAKE_INSTALL_PREFIX:PATH=$HOME/agave/opt/trilinos-12.10.1 \
 ..

make
make install
