#!/bin/bash
set -e

#SBATCH -n 1                         # number of cores
#SBATCH -t 0-12:00                   # wall time (D-HH:MM)
#SBATCH -o slurm.%j.out              # STDOUT (%j = JobId)
#SBATCH -e slurm.%j.err              # STDERR (%j = JobId)

module load mvapich2/2.3b-gnu-6x

CC=`which mpicc`
FC=`which mpifort`
CXX=`which mpicxx`

BUILD_DIR=../Build
HDF5_DIR=$HOME/agave/opt/hdf5-1.8.20

if test -d $BUILD_DIR; then echo "directory exists"; else mkdir $BUILD_DIR; fi 
cd $BUILD_DIR

curl "https://support.hdfgroup.org/ftp/lib-external/szip/2.1.1/src/szip-2.1.1.tar.gz" | tar -xz
curl "https://support.hdfgroup.org/ftp/HDF5/current18/src/hdf5-1.8.20.tar" | tar -x

cd szip*
./configure --prefix=$HDF5_DIR --disable-shared
make all
make install

cd $BUILD_DIR

cd hdf5*
./configure --prefix=$HDF5_DIR --disable-shared --enable-parallel --enable-fortran --enable-fortran2003
make all
make install
