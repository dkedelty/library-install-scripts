#!/bin/bash
set -e

BUILD_DIR=~/Downloads/tmp
FFTW_DIR=/opt/fftw-3.3.6
LAPACK_DIR=/opt/lapack-3.7.0
HYPRE_DIR=/opt/hypre-2.11.1

export CC=`which mpicc`
export CXX=`which mpicxx`
export FC=`which mpifort`
export F77=`which mpifort`
export MPICC=`which mpicc`

if test -d $BUILD_DIR; then echo "directory exists"; else mkdir $BUILD_DIR; fi 
cd $BUILD_DIR

curl "http://www.fftw.org/fftw-3.3.6-pl1.tar.gz" | tar -xz
curl "http://www.netlib.org/lapack/lapack-3.7.0.tgz" | tar -xz
curl "http://computation.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods/download/hypre-2.11.1.tar.gz" | tar -xz

cd fftw*

./configure --prefix=$FFTW_DIR --enable-mpi
make -j 2
make install

cd $BUILD_DIR

cd lapack*

cp make.inc.example make.inc
sed -i '' "s#FORTRAN  = gfortran#FORTRAN  = $FC#g" make.inc
sed -i '' "s#LOADER   = gfortran#LOADER   = $FC#g" make.inc
sed -i '' "s#CC = gcc#CC = $CC#g" make.inc
sed -i '' 's#BLASLIB      = ../../librefblas.a#BLASLIB      = ../../libblas.a#g' make.inc
make blaslib
make lapacklib
mkdir $LAPACK_DIR && mv *.a $LAPACK_DIR

cd $BUILD_DIR

cd hypre*/src
./configure --prefix=$HYPRE_DIR --with-blas-libs="libblas" --with-blas-lib-dirs=$LAPACK_DIR --with-lapack-libs="liblapack" --with-lapack-lib-dirs=$LAPACK_DIR --with-MPI
make -j 2
make install 


