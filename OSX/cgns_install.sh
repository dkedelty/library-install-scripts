#!/bin/bash
set -e

BUILD_DIR=~/Downloads/tmp
SZIP_DIR=/opt/szip-2.1
ZLIB_DIR=/opt/zlib-1.2.11
HDF5_DIR=/opt/hdf5-1.8.18
CGNS_DIR=/opt/cgns-3.3.0
MPI_DIR=/usr/local/openmpi-2.0.1

export CC=`which mpicc`
export CXX=`which mpicxx`
export FC=`which mpifort`
export F77=`which mpifort`
export MPICC=`which mpicc`

if test -d $BUILD_DIR; then echo "directory exists"; else mkdir $BUILD_DIR; fi 
cd $BUILD_DIR

curl "http://www.zlib.net/zlib-1.2.11.tar.gz" | tar -xz
cd zlib*

./configure --prefix=$ZLIB_DIR
make -j 2
make install

cd $BUILD_DIR

curl "https://support.hdfgroup.org/ftp/lib-external/szip/2.1/src/szip-2.1.tar.gz" | tar -xz
cd szip*

./configure --prefix=$SZIP_DIR
make -j 2
make install

cd $BUILD_DIR

curl "https://support.hdfgroup.org/ftp/HDF5/current18/src/hdf5-1.8.18.tar.gz" | tar -xz
cd hdf5*

./configure --prefix=$HDF5_DIR --with-zlib=$ZLIB_DIR --with-szip=$SZIP_DIR --disable-shared --enable-fortran --enable-fortran2003 --enable-parallel
make -j 2
make install

cd $BUILD_DIR

curl -L "https://github.com/CGNS/CGNS/archive/v3.3.0.tar.gz" | tar -xz
cd CGNS*/src

# This part likely still needs some work...
./configure --with-fortran --with-hdf5=$HDF5_DIR --with-zlib=$ZLIB_DIR/include/zlib.h --with-szip=$SZIP_DIR/lib/libsz.a --with-mpi=$OMPI_DIR --enable-parallel --prefix=$CGNS_DIR
sed -i '' 's#ZLIBLIB = /opt/zlib-1.2.11/include/libz.a#ZLIBLIB = /opt/zlib-1.2.11/lib/libz.a#g' make.defs
make -j 2
make install
