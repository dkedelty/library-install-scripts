#!/bin/bash
set -e

BUILD_DIR=~/Downloads/tmp
GCC_DIR=/usr/local/gcc-test
GCC_SUFFIX=-test
OMPI_DIR=/usr/local/gcc-test
echo Installing OpenMPI to $OMPI_DIR

if test -d $BUILD_DIR; then echo "directory exists"; else mkdir $BUILD_DIR; fi 
cd $BUILD_DIR

curl "https://www.open-mpi.org/software/ompi/v2.0/downloads/openmpi-2.0.1.tar.gz" | tar -xz

cd openmpi*
mkdir build && cd build
../configure CC=$GCC_DIR/bin/gcc$GCC_SUFFIX \
            CXX=$GCC_DIR/bin/g++$GCC_SUFFIX \
            F77=$GCC_DIR/bin/gfortran$GCC_SUFFIX \
            FC=$GCC_DIR/bin/gfortran$GCC_SUFFIX \
            --prefix=$OMPI_DIR
make -j 2
make install

cd ../
rm -rf openmpi*
