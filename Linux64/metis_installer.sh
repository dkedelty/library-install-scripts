#!/bin/bash
set -e

BUILD_DIR=~/Downloads/tmp
METIS_DIR=/opt/metis-5.1.0
PARMETIS_DIR=/opt/parmetis-4.0.3

if test -d $BUILD_DIR; then echo "directory exists"; else mkdir $BUILD_DIR; fi 
cd $BUILD_DIR

curl "http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz" | tar -xz

cd metis*

MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then
   # edit metis.h and redefine IDXTYPEWIDTH as 64
   sed -i 's/#define IDXTYPEWIDTH 32/#define IDXTYPEWIDTH 64/g' include/metis.h
   sed -i 's/#define REALTYPEWIDTH 32/#define REALTYPEWIDTH 64/g' include/metis.h
else
   echo "Not changing IDXTYPEWIDTH"
fi

# configure & install
make config CC=`which mpicc` CXX=`which mpicxx` FC=`which mpifort` F90=`which mpifort`  prefix=$METIS_DIR
make -j 12
make install

cd  $BUILD_DIR

curl "http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/parmetis-4.0.3.tar.gz" | tar -xz

cd parmetis*

if [ ${MACHINE_TYPE} == 'x86_64' ]; then
   # edit metis.h and redefine IDXTYPEWIDTH as 64
   sed -i 's/#define IDXTYPEWIDTH 32/#define IDXTYPEWIDTH 64/g' metis/include/metis.h
   sed -i 's/#define REALTYPEWIDTH 32/#define REALTYPEWIDTH 64/g' metis/include/metis.h
else
   echo "Not changing IDXTYPEWIDTH"
fi

# configure & install
make config CC=`which mpicc` CXX=`which mpicxx` FC=`which mpifort` F90=`which mpifort` prefix=$PARMETIS_DIR
make -j 12
make install

#cleanup
cd $BUILD_DIR
rm -rf metis* parmetis*
