#!/bin/bash
set -e

BUILD_DIR=~/Downloads/tmp
GCC_DIR=/usr/local/gcc-6.3.0
GCC_SUFFIX=-6.3.0
echo Installing GCC to $GCC_DIR

if test -d $BUILD_DIR; then echo "directory exists"; else mkdir $BUILD_DIR; fi 
cd $BUILD_DIR

curl "https://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.bz2" | tar -xj
curl "http://www.mpfr.org/mpfr-current/mpfr-3.1.6.tar.gz" | tar -xz
curl "http://isl.gforge.inria.fr/isl-0.18.tar.gz" | tar -xz
curl "ftp://ftp.gnu.org/gnu/mpc/mpc-1.0.3.tar.gz" | tar -xz
curl "http://www.netgull.com/gcc/releases/gcc-6.3.0/gcc-6.3.0.tar.gz" | tar -xz

cd gmp*
mkdir build && cd build
../configure --prefix=$GCC_DIR --enable-cxx
make -j 12
make install

cd ../../mpfr*
mkdir build && cd build
../configure --prefix=$GCC_DIR --with-gmp=$GCC_DIR
make -j 12
make install

cd ../../mpc*
mkdir build && cd build
../configure --prefix=$GCC_DIR \
             --with-gmp=$GCC_DIR \
             --with-mpfr=$GCC_DIR
make -j 12
make install

cd ../../isl*
mkdir build && cd build
../configure --prefix=$GCC_DIR --with-gmp-prefix=$GCC_DIR
make -j 12
make install

cd ../../gcc*
mkdir build && cd build
../configure --prefix=$GCC_DIR \
             --enable-checking=release \
             --with-gmp=$GCC_DIR \
             --with-mpfr=$GCC_DIR \
             --with-mpc=$GCC_DIR \
             --enable-languages=c,c++,fortran \
             --with-isl=$GCC_DIR \
             --program-suffix=$GCC_SUFFIX
make -j 12
make install

cd ../../
rm -rf gmp* mpfr* mpc* isl* gcc*
